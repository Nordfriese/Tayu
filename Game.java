import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;
import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.util.*;
import javax.imageio.ImageIO;
import javax.swing.*;

public class Game {
	public static final int NORTH = 0;
	public static final int EAST  = 1;
	public static final int SOUTH = 2;
	public static final int WEST  = 3;
	public static final int BOARD_DIMENSION = 19;
	public static final java.util.List<Integer> doubleCountIndices = Arrays.asList(2, BOARD_DIMENSION / 2, BOARD_DIMENSION - 2 - 1);

	public static enum GameType {
		kLocal,
		kHost,
		kClient
	}
	public static final int SOCKET_PORT = 7422;
	private static final String NETWORK_PROTOCOL = "PROTOCOL_VERSION:1";
	private static final String END_OF_WELCOME = "END_OF_WELCOME";
	private static final String CMD_END_GAME = "END_GAME";
	private static final String CMD_DRAW_BRICK = "DRAW_BRICK";
	private static final String CMD_PLACE_BRICK = "PLACE_BRICK";

	private static class SingleTile {
		public final boolean[] water;
		public SingleTile(boolean n, boolean e, boolean s, boolean w) {
			water = new boolean[] { n, e, s, w };
		}
		public SingleTile(boolean[] b) {
			water = b;
		}
		public int count() {
			int i = 0;
			for (boolean w : water) if (w) ++i;
			return i;
		}

		public static final SingleTile NS = new SingleTile(true, false, true, false);
		public static final SingleTile WE = new SingleTile(false, true, false, true);
		public static final SingleTile NE = new SingleTile(true, true, false, false);
		public static final SingleTile NW = new SingleTile(true, false, false, true);
		public static final SingleTile SE = new SingleTile(false, true, true, false);
		public static final SingleTile SW = new SingleTile(false, false, true, true);
		public static final SingleTile NSW = new SingleTile(true, false, true, true);
		public static final SingleTile NSE = new SingleTile(true, true, true, false);
		public static final SingleTile WEN = new SingleTile(true, true, false, true);
		public static final SingleTile WES = new SingleTile(false, true, true, true);
		public static final SingleTile __ = new SingleTile(false, false, false, false);
	};
	private static class Brick {
		/* Standard layout at rotation NORTH:
		 *  N
		 * W E   S1 S2 S3
		 *  S
		 */
		public final SingleTile[] tiles;
		public int rotation;
		public Brick(SingleTile s1, SingleTile s2, SingleTile s3) {
			tiles = new SingleTile[] { s1, s2, s3 };
			rotation = NORTH;
		}
	};

	public static final File kSaveFile = new File("save");

	private final GameType gametype;
	private final Menu menu;
	private final JFrame frame;
	private final JLabel display;

	private SingleTile[][] board;
	private final ArrayList<Brick> brickStack;

	private final String[] playerNames;
	private int currentPlayer;  // 0 plays N-S, 1 plays W-E
	private Brick currentBrick;

	private Point mousePos;
	private int tilesize, offX, offY;

	private ServerSocket hostSocket;
	private Socket clientSocket;

	private static enum FieldAssessment {
		kRelevant,
		kBlocked,
		kLandlocked,
		kInner,
	}
	private FieldAssessment[][] assessments;

	private static final Color kColorDark      = new Color(0x000673);
	private static final Color kColorLight     = new Color(0x5C65FF);
	private static final Color kColorVeryLight = new Color(0xB0D7F0);
	private static final Color kLandTile       = new Color(0xFFF585);
	private static final Color kColorBlocked   = new Color(0xFF9D9D);
	private static final Color kColorAllowed   = new Color(0xbf9BFFA2, true);
	private static final Color kColorForbidden = new Color(0xbf9E6570, true);

	private static void drawTile(Graphics2D g, Rectangle r, SingleTile tile, int rotation) {
		g.setColor(kLandTile);
		g.fill(r);
		if (tile.count() < 1) return;

		g.setColor(kColorLight);
		                                        g.fillRect(r.x + r.width / 3, r.y + r.height / 3, r.width / 3, r.height / 3);
		if (tile.water[(NORTH + 4 - rotation) % 4]) g.fillRect(r.x + r.width / 3, r.y               , r.width / 3, r.height / 2);
		if (tile.water[(SOUTH + 4 - rotation) % 4]) g.fillRect(r.x + r.width / 3, r.y + r.height / 2, r.width / 3, r.height / 2);
		if (tile.water[(WEST  + 4 - rotation) % 4]) g.fillRect(r.x              , r.y + r.height / 3, r.width / 2, r.height / 3);
		if (tile.water[(EAST  + 4 - rotation) % 4]) g.fillRect(r.x + r.width / 2, r.y + r.height / 3, r.width / 2, r.height / 3);
	}

	public synchronized void draw() {
		int w = display.getWidth();
		int h = display.getHeight();
		int whm = Math.max(w, h);

		BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = img.createGraphics();

		g.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, tilesize * 3 / 4));

		for (int i = 0; i < whm * 2; i++) {
			int c = 255 * i / (whm * 2);
			g.setColor(new Color(c, c, c));
			if (w > h)
				g.drawLine(i, 0, 0, i * h / w);
			else
				g.drawLine(i * w / h, 0, 0, i);
		}

		tilesize = Math.min(w / (BOARD_DIMENSION + 10), h / (BOARD_DIMENSION + 2));
		offX = (w - tilesize * (BOARD_DIMENSION + 10)) / 2 + tilesize;
		offY = (h - tilesize * (BOARD_DIMENSION + 2)) / 2 + tilesize;

		for (Integer i : doubleCountIndices) {
			g.setColor(currentPlayer == 0 ? kColorVeryLight : kColorLight);
			g.fillOval(offX + i * tilesize, offY - tilesize, tilesize, tilesize);
			g.fillOval(offX + i * tilesize, offY + tilesize * BOARD_DIMENSION, tilesize, tilesize);
			if (currentPlayer == 0) {
				g.setColor(kColorLight);
				g.fillPolygon(new int[] {
						offX + i * tilesize + tilesize * 1 / 4,
						offX + i * tilesize + tilesize * 3 / 4,
						offX + i * tilesize + tilesize * 2 / 4,
					}, new int[] {
						offY - tilesize * 1 / 4,
						offY - tilesize * 1 / 4,
						offY - tilesize * 3 / 4,
					}, 3);
				g.fillPolygon(new int[] {
						offX + i * tilesize + tilesize * 1 / 4,
						offX + i * tilesize + tilesize * 3 / 4,
						offX + i * tilesize + tilesize * 2 / 4,
					}, new int[] {
						offY + tilesize * BOARD_DIMENSION + tilesize * 1 / 4,
						offY + tilesize * BOARD_DIMENSION + tilesize * 1 / 4,
						offY + tilesize * BOARD_DIMENSION + tilesize * 3 / 4,
					}, 3);
			}

			g.setColor(currentPlayer == 1 ? kColorVeryLight : kColorLight);
			g.fillOval(offX - tilesize, offY + i * tilesize, tilesize, tilesize);
			g.fillOval(offX + BOARD_DIMENSION * tilesize, offY + i * tilesize, tilesize, tilesize);
			if (currentPlayer == 1) {
				g.setColor(kColorLight);
				g.fillPolygon(new int[] {
						offX - tilesize * 1 / 4,
						offX - tilesize * 1 / 4,
						offX - tilesize * 3 / 4,
					}, new int[] {
						offY + i * tilesize + tilesize * 1 / 4,
						offY + i * tilesize + tilesize * 3 / 4,
						offY + i * tilesize + tilesize * 2 / 4,
					}, 3);
				g.fillPolygon(new int[] {
						offX + tilesize * BOARD_DIMENSION + tilesize * 1 / 4,
						offX + tilesize * BOARD_DIMENSION + tilesize * 1 / 4,
						offX + tilesize * BOARD_DIMENSION + tilesize * 3 / 4,
					}, new int[] {
						offY + i * tilesize + tilesize * 1 / 4,
						offY + i * tilesize + tilesize * 3 / 4,
						offY + i * tilesize + tilesize * 2 / 4,
					}, 3);
			}
		}

		for (int x = 0; x < BOARD_DIMENSION; ++x) {
			for (int y = 0; y < BOARD_DIMENSION; ++y) {
				Rectangle r = new Rectangle(offX + x * tilesize, offY + y * tilesize, tilesize, tilesize);

				g.setColor(kColorDark);
				g.fill(r);
				g.setColor(kColorLight);
				g.draw(r);

				if (board[x][y] == null) {
					if (x == BOARD_DIMENSION / 2 && y == BOARD_DIMENSION / 2) {
						g.fillOval(r.x, r.y, r.width, r.height);
					}
					if (assessments != null && assessments[x][y] != FieldAssessment.kRelevant) {
						g.setColor(assessments[x][y] == FieldAssessment.kInner ? kColorLight : assessments[x][y] == FieldAssessment.kLandlocked ? kLandTile : kColorBlocked);
						g.fillRect(r.x + r.width / 3, r.y + r.height / 3, r.width / 3, r.height / 3);
					}
				} else {
					drawTile(g, r, board[x][y], NORTH);
				}
			}
		}

		String statusText;
		String plText = playerNames[currentPlayer] + "’s turn.";
		if (isWaitingForPlayerToJoin()) {
			plText = "Waiting for connection.";
			statusText = "";
		} else if (currentPlayerIsRemote()) {
			statusText = "Waiting for remote.";
		} else if (currentBrick != null) {
			statusText = "Place your brick.";
		} else {
			statusText = "Click to draw a brick.";
		}

		g.setColor(kColorDark);
		g.drawString(plText, offX + tilesize * (BOARD_DIMENSION + 2), offY +  2 * tilesize - tilesize / 4);
		g.drawString(statusText, offX + tilesize * (BOARD_DIMENSION + 2), offY +  3 * tilesize - tilesize / 4);

		if (currentBrick != null) {
			for (int i = 0; i < currentBrick.tiles.length; ++i) {
				drawTile(g, new Rectangle(offX + tilesize * (BOARD_DIMENSION + 3 + i), offY + 9 * tilesize, tilesize, tilesize), currentBrick.tiles[i], NORTH);
			}
		}

		int[] score = score();
		g.setColor(currentPlayer == 0 ? kColorLight : kColorDark);
		g.fillRect(offX + tilesize * (BOARD_DIMENSION + 2), offY + 5 * tilesize, 6 * tilesize, 2 * tilesize);

		g.setColor(currentPlayer != 0 ? kColorLight : kColorDark);
		g.drawString("↕ " + playerNames[0] + ":", offX + tilesize * (BOARD_DIMENSION + 3), offY +  6 * tilesize - tilesize / 4);
		g.drawString(score[NORTH] + "×" + score[SOUTH] + "=" + (score[NORTH] * score[SOUTH]),
				offX + tilesize * (BOARD_DIMENSION + 4), offY +  7 * tilesize - tilesize / 4);

		g.fillRect(offX + tilesize * (BOARD_DIMENSION + 2), offY + 12 * tilesize, 6 * tilesize, 2 * tilesize);

		g.setColor(currentPlayer == 0 ? kColorLight : kColorDark);
		g.drawString("↔ " + (playerNames[1] != null ? playerNames[1] + ":" : ""), offX + tilesize * (BOARD_DIMENSION + 3), offY + 13 * tilesize - tilesize / 4);
		g.drawString(score[WEST] + "×" + score[EAST] + "=" + (score[WEST] * score[EAST]),
				offX + tilesize * (BOARD_DIMENSION + 4), offY + 14 * tilesize - tilesize / 4);

		if (currentBrick != null && mousePos != null && !currentPlayerIsRemote()) {
			Rectangle[] r = new Rectangle[currentBrick.tiles.length];
			r[1] = new Rectangle(mousePos.x - tilesize / 2, mousePos.y - tilesize / 2, tilesize, tilesize);
			final Point coords = mousePosToCoords();
			g.setColor(mayPlace(coords, currentBrick.rotation) ? kColorAllowed : kColorForbidden);
			switch (currentBrick.rotation) {
				case NORTH:
					r[0] = new Rectangle(r[1].x - tilesize, r[1].y, tilesize, tilesize);
					r[2] = new Rectangle(r[1].x + tilesize, r[1].y, tilesize, tilesize);
					g.fillRect(offX + tilesize * (coords.x - 1), offY + tilesize * coords.y, 3 * tilesize, tilesize);
					break;
				case SOUTH:
					r[2] = new Rectangle(r[1].x - tilesize, r[1].y, tilesize, tilesize);
					r[0] = new Rectangle(r[1].x + tilesize, r[1].y, tilesize, tilesize);
					g.fillRect(offX + tilesize * (coords.x - 1), offY + tilesize * coords.y, 3 * tilesize, tilesize);
					break;
				case EAST:
					r[0] = new Rectangle(r[1].x, r[1].y - tilesize, tilesize, tilesize);
					r[2] = new Rectangle(r[1].x, r[1].y + tilesize, tilesize, tilesize);
					g.fillRect(offX + tilesize * coords.x, offY + tilesize * (coords.y - 1), tilesize, 3 * tilesize);
					break;
				case WEST:
					r[2] = new Rectangle(r[1].x, r[1].y - tilesize, tilesize, tilesize);
					r[0] = new Rectangle(r[1].x, r[1].y + tilesize, tilesize, tilesize);
					g.fillRect(offX + tilesize * coords.x, offY + tilesize * (coords.y - 1), tilesize, 3 * tilesize);
					break;
			}
			for (int i = 0; i < r.length; ++i) drawTile(g, r[i], currentBrick.tiles[i], currentBrick.rotation);
		}

		display.setIcon(new ImageIcon(img));
	}

	private Point mousePosToCoords() {
		return mousePos == null ? null : new Point((mousePos.x - offX) / tilesize, (mousePos.y - offY) / tilesize);
	}

	public boolean mayPlace(Point coords, int rotation) {
		if (currentBrick == null || coords == null) return false;

		if (coords.x < 0 || coords.y < 0 || coords.x >= BOARD_DIMENSION || coords.y >= BOARD_DIMENSION) return false;
		if (board[coords.x][coords.y] != null) return false;

		boolean hasAdjacentWater = false;
		switch (rotation) {
			case NORTH:
			case SOUTH:
				if (coords.x < 1 || coords.x + 1 >= BOARD_DIMENSION) return false;
				if (board[coords.x - 1][coords.y] != null || board[coords.x + 1][coords.y] != null) return false;
				hasAdjacentWater |= coords.y == BOARD_DIMENSION / 2 && coords.x - 1 <= BOARD_DIMENSION / 2 && coords.x + 1 >= BOARD_DIMENSION / 2;
				break;
			case WEST:
			case EAST:
				if (coords.y < 1 || coords.y + 1 >= BOARD_DIMENSION) return false;
				if (board[coords.x][coords.y - 1] != null || board[coords.x][coords.y + 1] != null) return false;
				hasAdjacentWater |= coords.x == BOARD_DIMENSION / 2 && coords.y - 1 <= BOARD_DIMENSION / 2 && coords.y + 1 >= BOARD_DIMENSION / 2;
				break;
		}

		switch (rotation) {
			case NORTH:
				// check upper boundary
				if (coords.y > 0) {
					for (int i = 0; i < currentBrick.tiles.length; ++i) {
						if (board[coords.x - 1 + i][coords.y - 1] != null) {
							boolean b1 = currentBrick.tiles[i].water[NORTH];
							boolean b2 = board[coords.x - 1 + i][coords.y - 1].water[SOUTH];
							if (b1 != b2) return false;
							hasAdjacentWater |= b1;
						}
					}
				}

				// check lower boundary
				if (coords.y + 1 < BOARD_DIMENSION) {
					for (int i = 0; i < currentBrick.tiles.length; ++i) {
						if (board[coords.x - 1 + i][coords.y + 1] != null) {
							boolean b1 = currentBrick.tiles[i].water[SOUTH];
							boolean b2 = board[coords.x - 1 + i][coords.y + 1].water[NORTH];
							if (b1 != b2) return false;
							hasAdjacentWater |= b1;
						}
					}
				}

				// check left boundary
				if (coords.x > 1 && board[coords.x - 2][coords.y] != null) {
					boolean b1 = currentBrick.tiles[0].water[WEST];
					boolean b2 = board[coords.x - 2][coords.y].water[EAST];
					if (b1 != b2) return false;
					hasAdjacentWater |= b1;
				}

				// check right boundary
				if (coords.x + 2 < BOARD_DIMENSION && board[coords.x + 2][coords.y] != null) {
					boolean b1 = currentBrick.tiles[2].water[EAST];
					boolean b2 = board[coords.x + 2][coords.y].water[WEST];
					if (b1 != b2) return false;
					hasAdjacentWater |= b1;
				}

				break;

			case SOUTH:
				// check upper boundary
				if (coords.y > 0) {
					for (int i = 0; i < currentBrick.tiles.length; ++i) {
						if (board[coords.x + 1 - i][coords.y - 1] != null) {
							boolean b1 = currentBrick.tiles[i].water[SOUTH];
							boolean b2 = board[coords.x + 1 - i][coords.y - 1].water[SOUTH];
							if (b1 != b2) return false;
							hasAdjacentWater |= b1;
						}
					}
				}

				// check lower boundary
				if (coords.y + 1 < BOARD_DIMENSION) {
					for (int i = 0; i < currentBrick.tiles.length; ++i) {
						if (board[coords.x + 1 - i][coords.y + 1] != null) {
							boolean b1 = currentBrick.tiles[i].water[NORTH];
							boolean b2 = board[coords.x + 1 - i][coords.y + 1].water[NORTH];
							if (b1 != b2) return false;
							hasAdjacentWater |= b1;
						}
					}
				}

				// check left boundary
				if (coords.x > 1 && board[coords.x - 2][coords.y] != null) {
					boolean b1 = currentBrick.tiles[2].water[EAST];
					boolean b2 = board[coords.x - 2][coords.y].water[EAST];
					if (b1 != b2) return false;
					hasAdjacentWater |= b1;
				}

				// check right boundary
				if (coords.x + 2 < BOARD_DIMENSION && board[coords.x + 2][coords.y] != null) {
					boolean b1 = currentBrick.tiles[0].water[WEST];
					boolean b2 = board[coords.x + 2][coords.y].water[WEST];
					if (b1 != b2) return false;
					hasAdjacentWater |= b1;
				}

				break;

			case EAST:
				// check upper boundary
				if (coords.y > 1 && board[coords.x][coords.y - 2] != null) {
					boolean b1 = currentBrick.tiles[0].water[WEST];
					boolean b2 = board[coords.x][coords.y - 2].water[SOUTH];
					if (b1 != b2) return false;
					hasAdjacentWater |= b1;
				}

				// check lower boundary
				if (coords.y + 2 < BOARD_DIMENSION && board[coords.x][coords.y + 2] != null) {
					boolean b1 = currentBrick.tiles[2].water[EAST];
					boolean b2 = board[coords.x][coords.y + 2].water[NORTH];
					if (b1 != b2) return false;
					hasAdjacentWater |= b1;
				}

				// check left boundary
				if (coords.x > 0) {
					for (int i = 0; i < currentBrick.tiles.length; ++i) {
						if (board[coords.x - 1][coords.y + i - 1] != null) {
							boolean b1 = currentBrick.tiles[i].water[SOUTH];
							boolean b2 = board[coords.x - 1][coords.y + i - 1].water[EAST];
							if (b1 != b2) return false;
							hasAdjacentWater |= b1;
						}
					}
				}

				// check right boundary
				if (coords.x + 1 < BOARD_DIMENSION) {
					for (int i = 0; i < currentBrick.tiles.length; ++i) {
						if (board[coords.x + 1][coords.y + i - 1] != null) {
							boolean b1 = currentBrick.tiles[i].water[NORTH];
							boolean b2 = board[coords.x + 1][coords.y + i - 1].water[WEST];
							if (b1 != b2) return false;
							hasAdjacentWater |= b1;
						}
					}
				}

				break;

			case WEST:
				// check upper boundary
				if (coords.y > 1 && board[coords.x][coords.y - 2] != null) {
					boolean b1 = currentBrick.tiles[2].water[EAST];
					boolean b2 = board[coords.x][coords.y - 2].water[SOUTH];
					if (b1 != b2) return false;
					hasAdjacentWater |= b1;
				}

				// check lower boundary
				if (coords.y + 2 < BOARD_DIMENSION && board[coords.x][coords.y + 2] != null) {
					boolean b1 = currentBrick.tiles[0].water[WEST];
					boolean b2 = board[coords.x][coords.y + 2].water[NORTH];
					if (b1 != b2) return false;
					hasAdjacentWater |= b1;
				}

				// check left boundary
				if (coords.x > 0) {
					for (int i = 0; i < currentBrick.tiles.length; ++i) {
						if (board[coords.x - 1][coords.y - i + 1] != null) {
							boolean b1 = currentBrick.tiles[i].water[NORTH];
							boolean b2 = board[coords.x - 1][coords.y - i + 1].water[EAST];
							if (b1 != b2) return false;
							hasAdjacentWater |= b1;
						}
					}
				}

				// check right boundary
				if (coords.x + 1 < BOARD_DIMENSION) {
					for (int i = 0; i < currentBrick.tiles.length; ++i) {
						if (board[coords.x + 1][coords.y - i + 1] != null) {
							boolean b1 = currentBrick.tiles[i].water[SOUTH];
							boolean b2 = board[coords.x + 1][coords.y - i + 1].water[WEST];
							if (b1 != b2) return false;
							hasAdjacentWater |= b1;
						}
					}
				}

				break;
		}

		return hasAdjacentWater;
	}

	private int[] score() {
		int[] openings = new int[4];
		for (int i = 0; i < BOARD_DIMENSION; ++i) {
			final boolean dc = doubleCountIndices.contains(i);
			if (board[i][0] != null && board[i][0].water[NORTH]) openings[NORTH] += dc ? 2 : 1;
			if (board[i][BOARD_DIMENSION - 1] != null && board[i][BOARD_DIMENSION - 1].water[SOUTH]) openings[SOUTH] += dc ? 2 : 1;
			if (board[0][i] != null && board[0][i].water[WEST]) openings[WEST] += dc ? 2 : 1;
			if (board[BOARD_DIMENSION - 1][i] != null && board[BOARD_DIMENSION - 1][i].water[EAST]) openings[EAST] += dc ? 2 : 1;
		}
		return openings;
	}

	private void recalcAssessments() {
		if (assessments == null) {
			assessments = new FieldAssessment[board.length][board[0].length];
			for (int i = 0; i < assessments.length; ++i) for (int j = 0; j < assessments[i].length; ++j) assessments[i][j] = FieldAssessment.kRelevant;
		}

		for (int i = 0; i < assessments.length; ++i) for (int j = 0; j < assessments[i].length; ++j) {
			if (board[i][j] != null) {
				assessments[i][j] = board[i][j].count() > 0 ? FieldAssessment.kInner : FieldAssessment.kLandlocked;
			}
		}

		if (board[BOARD_DIMENSION / 2][BOARD_DIMENSION / 2] == null) return;  // Not yet initialized.

		// Step 1: Find blocked tiles.
		for (int i = 0; i < assessments.length; ++i) for (int j = 0; j < assessments[i].length; ++j) {
			if (board[i][j] != null || assessments[i][j] == FieldAssessment.kBlocked) continue;

			boolean canPlace = false;
			boolean w1 = i > 0 && assessments[i - 1][j] != FieldAssessment.kBlocked && board[i - 1][j] == null;
			boolean w2 = i > 1 && assessments[i - 2][j] != FieldAssessment.kBlocked && board[i - 2][j] == null;
			boolean e1 = i + 1 < assessments.length && assessments[i + 1][j] != FieldAssessment.kBlocked && board[i + 1][j] == null;
			boolean e2 = i + 2 < assessments.length && assessments[i + 2][j] != FieldAssessment.kBlocked && board[i + 2][j] == null;
			boolean s1 = j + 1 < assessments[i].length && assessments[i][j + 1] != FieldAssessment.kBlocked && board[i][j + 1] == null;
			boolean s2 = j + 2 < assessments[i].length && assessments[i][j + 2] != FieldAssessment.kBlocked && board[i][j + 2] == null;
			boolean n1 = j > 0 && assessments[i][j - 1] != FieldAssessment.kBlocked && board[i][j - 1] == null;
			boolean n2 = j > 1 && assessments[i][j - 2] != FieldAssessment.kBlocked && board[i][j - 2] == null;

			Point curP = new Point(i, j);
			ArrayList<java.util.List<Point>> candidateRegions = new ArrayList<>();
			if (w1 && w2) candidateRegions.add(java.util.List.of(curP, new Point(i - 1, j), new Point(i - 2, j)));
			if (e1 && e2) candidateRegions.add(java.util.List.of(curP, new Point(i + 1, j), new Point(i + 2, j)));
			if (w1 && e1) candidateRegions.add(java.util.List.of(curP, new Point(i - 1, j), new Point(i + 1, j)));
			if (n1 && n2) candidateRegions.add(java.util.List.of(curP, new Point(i, j - 1), new Point(i, j - 2)));
			if (s1 && s2) candidateRegions.add(java.util.List.of(curP, new Point(i, j + 1), new Point(i, j + 2)));
			if (n1 && s1) candidateRegions.add(java.util.List.of(curP, new Point(i, j - 1), new Point(i, j + 1)));

			for (java.util.List<Point> candidates : candidateRegions) {
				int canals = 0;
				int openEdges = 0;

				for (Point p : candidates) {
					if (!candidates.contains(new Point(p.x - 1, p.y))) {
						if (p.x > 0) {
							if (board[p.x - 1][p.y] == null) {
								++openEdges;
							} else if (board[p.x - 1][p.y].water[EAST]) {
								++canals;
							}
						} else {
							++openEdges;
						}
					}

					if (!candidates.contains(new Point(p.x + 1, p.y))) {
						if (p.x + 1 < board.length) {
							if (board[p.x + 1][p.y] == null) {
								++openEdges;
							} else if (board[p.x + 1][p.y].water[WEST]) {
								++canals;
							}
						} else {
							++openEdges;
						}
					}

					if (!candidates.contains(new Point(p.x, p.y - 1))) {
						if (p.y > 0) {
							if (board[p.x][p.y - 1] == null) {
								++openEdges;
							} else if (board[p.x][p.y - 1].water[SOUTH]) {
								++canals;
							}
						} else {
							++openEdges;
						}
					}

					if (!candidates.contains(new Point(p.x, p.y + 1))) {
						if (p.y + 1 < board[p.x].length) {
							if (board[p.x][p.y + 1] == null) {
								++openEdges;
							} else if (board[p.x][p.y + 1].water[NORTH]) {
								++canals;
							}
						} else {
							++openEdges;
						}
					}
				}

				if (canals <= 3 && canals + openEdges >= 3) {
					canPlace = true;
					break;
				}
			}

			if (!canPlace) {
				assessments[i][j] = FieldAssessment.kBlocked;
			}
		}

		// Step 2: Find landlocked and inner regions.
		Set<Point> seeds = new HashSet<>();
		for (int i = 0; i < assessments.length; ++i) for (int j = 0; j < assessments[i].length; ++j) {
			if (assessments[i][j] != FieldAssessment.kRelevant && assessments[i][j] != FieldAssessment.kInner || seeds.contains(new Point(i, j))) continue;

			boolean foundCanal = false;
			boolean foundEdge = false;
			Queue<Point> queue = new ArrayDeque<>();
			Set<Point> visited = new HashSet<>();
			queue.add(new Point(i, j));

			while (!queue.isEmpty()) {
				Point p = queue.remove();
				if (visited.contains(p) || board[p.x][p.y] != null || assessments[p.x][p.y] == FieldAssessment.kBlocked) continue;

				if (p.x <= 0 || p.y <= 0 || p.x >= assessments.length - 1 || p.y >= assessments[p.x].length - 1) {
					foundEdge = true;
				}

				if (
						(p.y < board[p.x].length - 1 && board[p.x][p.y + 1] != null && board[p.x][p.y + 1].water[NORTH]) ||
						(p.y > 0 && board[p.x][p.y - 1] != null && board[p.x][p.y - 1].water[SOUTH]) ||
						(p.x > 0 && board[p.x - 1][p.y] != null && board[p.x - 1][p.y].water[EAST]) ||
						(p.x < board.length - 1 && board[p.x + 1][p.y] != null && board[p.x + 1][p.y].water[WEST])
				) {
					foundCanal = true;
				}

				visited.add(p);
				if (p.x > 0) queue.add(new Point(p.x - 1, p.y));
				if (p.x < board.length - 1) queue.add(new Point(p.x + 1, p.y));
				if (p.y > 0) queue.add(new Point(p.x, p.y - 1));
				if (p.y < board[p.x].length - 1) queue.add(new Point(p.x, p.y + 1));
			}

			if (!foundEdge || !foundCanal) {
				seeds.addAll(visited);
				for (Point p : visited) {
					if (assessments[p.x][p.y] != FieldAssessment.kBlocked) {
						assessments[p.x][p.y] = foundCanal ? FieldAssessment.kInner : FieldAssessment.kLandlocked;
					}
				}
			}
		}
	}

	private void gameOver() {
		draw();

		int[] openings = score();
		int points1 = openings[NORTH] * openings[SOUTH];
		int points2 = openings[WEST ] * openings[EAST ];
		JOptionPane.showMessageDialog(frame,
				String.format("The game has ended.\n\n" + playerNames[0] + " (N-S) has a score of %d × %d = %d\n" + playerNames[0] + " (W-E) has a score of %d × %d = %d\n\n%s",
						openings[NORTH], openings[SOUTH], points1,
						openings[WEST], openings[EAST], points2,
						points1 == points2 ? "The game is a draw!" : (playerNames[points2 > points1 ? 1 : 0] + " wins!")
				), "Game Over", JOptionPane.INFORMATION_MESSAGE);
		endGame(false);
	}

	private void doDrawBrick() {
		currentBrick = brickStack.remove(0);
		boolean canPlace = false;
		for (int x = 0; x < BOARD_DIMENSION && !canPlace; ++x)
			for (int y = 0; y < BOARD_DIMENSION && !canPlace; ++y)
				for (int r = 0; r < 4 && !canPlace; ++r)
					canPlace |= mayPlace(new Point(x, y), r);
		if (!canPlace) gameOver();
	}

	private void doPlaceBrick(Point c) {
		switch (currentBrick.rotation) {
			case NORTH:
				board[c.x - 1][c.y] = currentBrick.tiles[0];
				board[c.x    ][c.y] = currentBrick.tiles[1];
				board[c.x + 1][c.y] = currentBrick.tiles[2];
				break;
			case SOUTH:
				board[c.x    ][c.y] = new SingleTile(currentBrick.tiles[1].water[SOUTH], currentBrick.tiles[1].water[WEST],
				                                     currentBrick.tiles[1].water[NORTH], currentBrick.tiles[1].water[EAST]);
				board[c.x + 1][c.y] = new SingleTile(currentBrick.tiles[0].water[SOUTH], currentBrick.tiles[0].water[WEST],
				                                     currentBrick.tiles[0].water[NORTH], currentBrick.tiles[0].water[EAST]);
				board[c.x - 1][c.y] = new SingleTile(currentBrick.tiles[2].water[SOUTH], currentBrick.tiles[2].water[WEST],
				                                     currentBrick.tiles[2].water[NORTH], currentBrick.tiles[2].water[EAST]);
				break;
			case EAST:
				board[c.x][c.y    ] = new SingleTile(currentBrick.tiles[1].water[WEST], currentBrick.tiles[1].water[NORTH],
				                                     currentBrick.tiles[1].water[EAST], currentBrick.tiles[1].water[SOUTH]);
				board[c.x][c.y - 1] = new SingleTile(currentBrick.tiles[0].water[WEST], currentBrick.tiles[0].water[NORTH],
				                                     currentBrick.tiles[0].water[EAST], currentBrick.tiles[0].water[SOUTH]);
				board[c.x][c.y + 1] = new SingleTile(currentBrick.tiles[2].water[WEST], currentBrick.tiles[2].water[NORTH],
				                                     currentBrick.tiles[2].water[EAST], currentBrick.tiles[2].water[SOUTH]);
				break;
			case WEST:
				board[c.x][c.y    ] = new SingleTile(currentBrick.tiles[1].water[EAST], currentBrick.tiles[1].water[SOUTH],
				                                     currentBrick.tiles[1].water[WEST], currentBrick.tiles[1].water[NORTH]);
				board[c.x][c.y + 1] = new SingleTile(currentBrick.tiles[0].water[EAST], currentBrick.tiles[0].water[SOUTH],
				                                     currentBrick.tiles[0].water[WEST], currentBrick.tiles[0].water[NORTH]);
				board[c.x][c.y - 1] = new SingleTile(currentBrick.tiles[2].water[EAST], currentBrick.tiles[2].water[SOUTH],
				                                     currentBrick.tiles[2].water[WEST], currentBrick.tiles[2].water[NORTH]);
				break;
		}

		currentBrick = null;
		++currentPlayer;
		currentPlayer %= 2;
		recalcAssessments();

		boolean hasRelevant = false;
		for (int i = 0; i < assessments.length && !hasRelevant; ++i) for (int j = 0; j < assessments[i].length && !hasRelevant; ++j) {
			hasRelevant |= assessments[i][j] == FieldAssessment.kRelevant;
		}

		if (!hasRelevant || brickStack.isEmpty()) gameOver();
	}

	private static void doSave(PrintWriter write, SingleTile tile) throws Exception {
		int val = 0;
		for (int i = 0; i < tile.water.length; ++i) {
			if (tile.water[i]) {
				val |= 1 << i;
			}
		}
		write.print(val);
	}

	private static void doSave(PrintWriter write, Brick brick) throws Exception {
		write.print(brick.rotation);
		for (SingleTile tile : brick.tiles) {
			write.print(',');
			doSave(write, tile);
		}
	}

	private static SingleTile doLoadTile(String text) {
		int val = Integer.valueOf(text);
		boolean[] water = new boolean[4];
		for (int i = 0; i < water.length; ++i) water[i] = (val & (1 << i)) != 0;
		return new SingleTile(water);
	}

	private static Brick doLoadBrick(String text) {
		String[] data = text.split(",");
		Brick brick = new Brick(doLoadTile(data[1]), doLoadTile(data[2]), doLoadTile(data[3]));
		brick.rotation = Integer.valueOf(data[0]);
		return brick;
	}

	private void saveToFile() {
		try {
			PrintWriter write = new PrintWriter(kSaveFile);
			doSave(write);
			write.close();
		} catch (Exception e) {
			System.out.println("Unable to save game! " + e);
			e.printStackTrace();
		}
	}
	private void doSave(PrintWriter write) throws Exception {
		write.println(playerNames[0]);
		write.println(playerNames[1]);
		write.println(currentPlayer + " " + board.length + " " + board[0].length + " " + brickStack.size() + " " + (currentBrick != null ? 1 : 0));

		for (SingleTile[] tt : board) {
			for (SingleTile tile : tt) {
				if (tile != null) {
					doSave(write, tile);
				} else {
					write.print('-');
				}
				write.print(' ');
			}
			write.println();
		}

		if (currentBrick != null) {
			doSave(write, currentBrick);
			write.println();
		}
		for (Brick b : brickStack) {
			doSave(write, b);
			write.print(' ');
		}
		write.println();
	}

	private String loadFromFile() {
		try {
			java.util.List<String> lines = Files.readAllLines(kSaveFile.toPath());
			doLoad(lines);
		} catch (Exception e) {
			System.out.println("Unable to load game! " + e);
			e.printStackTrace();
			return e.getMessage();
		}
		return null;
	}

	private void doLoad(java.util.List<String> lines) throws Exception {
		playerNames[0] = lines.remove(0);
		playerNames[1] = lines.remove(0);

		String[] data = lines.remove(0).split(" ");
		int index = 0;
		currentPlayer = Integer.valueOf(data[index++]);
		int bl1 = Integer.valueOf(data[index++]);
		int bl2 = Integer.valueOf(data[index++]);
		int nb = Integer.valueOf(data[index++]);
		int hasBrick = Integer.valueOf(data[index++]);

		board = new SingleTile[bl1][bl2];
		for (int i = 0; i < bl1; ++i) {
			data = lines.remove(0).split(" ");
			for (int j = 0; j < bl2; ++j) {
				if (!data[j].equals("-")) {
					board[i][j] = doLoadTile(data[j]);
				}
			}
		}

		if (hasBrick != 0) currentBrick = doLoadBrick(lines.remove(0));

		data = lines.remove(0).split(" ");
		brickStack.clear();
		for (String d : data) brickStack.add(doLoadBrick(d));

		assessments = null;
		recalcAssessments();
	}

	public boolean currentPlayerIsRemote() {
		return (gametype == GameType.kHost && currentPlayer == 1) || (gametype == GameType.kClient && currentPlayer == 0);
	}

	public boolean isWaitingForPlayerToJoin() {
		for (String str : playerNames) if (str == null) return true;
		return false;
	}

	private void writeToNetwork(Object... msgs) {
		if (clientSocket == null) return;
		try {
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			for (Object msg : msgs) out.println(msg);
		} catch (Exception e) {
			System.out.println("Unable to write on network! " + e);
			JOptionPane.showMessageDialog(frame, "Error communicating with remote:\n\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			endGame(true);
		}
	}

	public void endGame(boolean save) {
		if (save) {
			if (!isWaitingForPlayerToJoin()) saveToFile();
		} else {
			kSaveFile.delete();
		}

		try {
			writeToNetwork(CMD_END_GAME);
			if (clientSocket != null) {
				clientSocket.close();
				clientSocket = null;
			}
			if (hostSocket != null) {
				hostSocket.close();
				hostSocket = null;
			}
		} catch (Exception e) {
			System.out.println("Unable to close socket! " + e);
		}

		frame.dispose();
		menu.setVisible(true);
	}

	public Game(Menu menuframe, String p1name, String p2name, boolean load, GameType type) {
		menu = menuframe;
		gametype = type;
		frame = new JFrame("Tayü");
		display = new JLabel();
		String error = null;

		try {
			switch (type) {
				case kHost:
					p2name = null;
					hostSocket = new ServerSocket(SOCKET_PORT);
					break;

				case kClient:
					clientSocket = new Socket(p1name, SOCKET_PORT);
					p1name = null;
					break;

				default:
					break;
			}
		} catch (Exception e) {
			System.out.println("Unable to create socket! " + e);
			error = "Unable to create network socket:\n\n" + e.getMessage();
		}

		playerNames = new String[] { p1name, p2name };
		board = new SingleTile[BOARD_DIMENSION][BOARD_DIMENSION];
		brickStack = new ArrayList<>();

		for (int i = 0; i < 4; ++i) {
			for (Brick b : new Brick[] {
				new Brick(SingleTile.SE, SingleTile.WES, SingleTile.SW),
				new Brick(SingleTile.NSW, SingleTile.__, SingleTile.__),
				new Brick(SingleTile.NE, SingleTile.NSW, SingleTile.__),
				new Brick(SingleTile.NE, SingleTile.WEN, SingleTile.SW),

				new Brick(SingleTile.__, SingleTile.SE, SingleTile.WEN),
				new Brick(SingleTile.SE, SingleTile.WE, SingleTile.NSW),
				new Brick(SingleTile.__, SingleTile.NSE, SingleTile.NW),
				new Brick(SingleTile.WES, SingleTile.WE, SingleTile.WE),

				new Brick(SingleTile.WEN, SingleTile.SW, SingleTile.__),
				new Brick(SingleTile.NSE, SingleTile.WE, SingleTile.SW),
				new Brick(SingleTile.WES, SingleTile.WE, SingleTile.SW),
				new Brick(SingleTile.WE, SingleTile.WES, SingleTile.WE),

				new Brick(SingleTile.NE, SingleTile.WES, SingleTile.WE),
				new Brick(SingleTile.WE, SingleTile.WES, SingleTile.SW),
				new Brick(SingleTile.WE, SingleTile.WE, SingleTile.NSW),
				new Brick(SingleTile.WEN, SingleTile.WE, SingleTile.WE),

				new Brick(SingleTile.NSE, SingleTile.NW, SingleTile.__),
				new Brick(SingleTile.SE, SingleTile.WE, SingleTile.WES),
				new Brick(SingleTile.SE, SingleTile.WEN, SingleTile.WE),
				new Brick(SingleTile.NE, SingleTile.WES, SingleTile.NW),

				new Brick(SingleTile.__, SingleTile.NSE, SingleTile.WE),
				new Brick(SingleTile.WES, SingleTile.SW, SingleTile.__),
				new Brick(SingleTile.NSE, SingleTile.SW, SingleTile.__),
				new Brick(SingleTile.SE, SingleTile.WE, SingleTile.WEN),

				new Brick(SingleTile.WE, SingleTile.WEN, SingleTile.NW),
				new Brick(SingleTile.SE, SingleTile.WES, SingleTile.NW),
				new Brick(SingleTile.NE, SingleTile.WE, SingleTile.WES),
				new Brick(SingleTile.WEN, SingleTile.NW, SingleTile.__),
			}) {
				brickStack.add((int)(Math.random() * (brickStack.size() + 1)), b);
			}
		}

		currentPlayer = 0;
		currentBrick = null;

		if (load) {
			String msg = loadFromFile();
			if (msg != null) error = "Savegame failed to load:\n\n" + msg;
		}

		display.setPreferredSize(new Dimension(800, 600));
		frame.add(display);
		display.addKeyListener(new KeyAdapter() {
			@Override public void keyPressed(KeyEvent e) {
				draw();
			}
		});
		display.addMouseListener(new MouseAdapter() {
			@Override public void mousePressed(MouseEvent m) {
				if (isWaitingForPlayerToJoin() || currentPlayerIsRemote()) return;

				mousePos = m.getPoint();
				final Point c = mousePosToCoords();
				if (currentBrick == null) {
					writeToNetwork(CMD_DRAW_BRICK);
					doDrawBrick();
				} else if (mayPlace(c, currentBrick.rotation)) {
					writeToNetwork(CMD_PLACE_BRICK, c.x, c.y, currentBrick.rotation);
					doPlaceBrick(c);
				}
				draw();
			}
		});
		display.addMouseWheelListener(new MouseAdapter() {
			@Override public void mouseWheelMoved(MouseWheelEvent w) {
				if (currentBrick != null) {
					currentBrick.rotation -= w.getWheelRotation();
					while (currentBrick.rotation < 0) currentBrick.rotation += 4;
					currentBrick.rotation %= 4;
				}
				draw();
			}
		});
		display.addMouseMotionListener(new MouseAdapter() {
			@Override public void mouseMoved(MouseEvent m) {
				mousePos = m.getPoint();
				if (currentBrick != null) draw();
			}
		});
		display.addComponentListener(new ComponentAdapter() {
			@Override public void componentResized(ComponentEvent e) {
				draw();
			}
		});
		frame.addWindowListener(new WindowAdapter() {
			@Override public void windowClosing(WindowEvent e) {
				endGame(true);
			}
		});

		if (error == null) {
			display.setFocusable(true);
			frame.pack();
			frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			frame.setLocationRelativeTo(null);
			frame.setVisible(true);
		} else {
			JOptionPane.showMessageDialog(frame, error, "Error", JOptionPane.ERROR_MESSAGE);
			endGame(false);
		}

		if (gametype != GameType.kLocal) {
			new Thread(() -> {
				boolean savegame = false;
				try {
					if (gametype == GameType.kHost) clientSocket = hostSocket.accept();

					PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
					BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

					if (gametype == GameType.kHost) {  // Host sends welcome message and waits for answer.
						out.println(NETWORK_PROTOCOL);
						out.println(load);

						doSave(out);
						out.println(END_OF_WELCOME);

						playerNames[1] = in.readLine();
					} else {  // Client waits for welcome message and sends answer.
						String protocol = in.readLine();
						if (!NETWORK_PROTOCOL.equals(protocol)) throw new Exception("Wrong protocol version: excepted " + NETWORK_PROTOCOL + ", received " + protocol);

						boolean isLoad = Boolean.parseBoolean(in.readLine());
						String ownName = playerNames[1];

						ArrayList<String> lines = new ArrayList<>();
						for (;;) {
							String msg = in.readLine();
							if (msg == null) throw new Exception("Connection lost");
							if (msg.equals(END_OF_WELCOME)) break;
							lines.add(msg);
						}
						doLoad(lines);

						if (!isLoad) playerNames[1] = ownName;
						out.println(playerNames[1]);
					}

					draw();

					savegame = true;
					for (;;) {
						String msg = in.readLine();
						if (msg == null) throw new Exception("Connection lost");

						switch (msg) {
							case CMD_END_GAME:
								return;

							case CMD_DRAW_BRICK:
								doDrawBrick();
								break;

							case CMD_PLACE_BRICK: {
								int x = Integer.valueOf(in.readLine());
								int y = Integer.valueOf(in.readLine());
								currentBrick.rotation = Integer.valueOf(in.readLine());
								doPlaceBrick(new Point(x, y));
								break;
							}

							default:
								throw new Exception("Received unknown command " + msg);
						}

						draw();
					}

				} catch (Exception e) {
					System.out.println("Error in client communication! " + e);
					JOptionPane.showMessageDialog(frame,
							(savegame ? "Error in communication with client:\n\n" : "Error waiting for client to join:\n\n") + e.getMessage(),
							"Error", JOptionPane.ERROR_MESSAGE);
					endGame(savegame);
				}

			}).start();
		}
	}

	public static void main(String[] args) {
		new Menu();
	}
}
