# Tayü

Tayü is a board game for two players to build canals to connect their two sides of the game board, while hindering the opponent.

The game can be played both locally and via network. Written in Java.

## Usage

To fetch and compile:

```sh
git clone https://codeberg.org/Nordfriese/Tayu.git
cd Tayu
javac *.java
```

To run:

```sh
java Game
```

Requires a recent Java Development Kit. I recommend OpenJDK 17 or newer.

## The Game

The first player tries to connect the upper and lower edge of the game board; the second player tries to connect the left and right side.

At the beginning of your turn, click to draw a brick. Each waterway brick covers 3 tiles and has 3 openings. Use the mouse wheel to rotate the brick.
The first brick must be placed in the center of the game board. Every other brick must be placed in such a way that its waterway is connected to at least one existing waterway.
Bricks cannot be placed where they overlap with an already placed brick, and it is not allowed for a waterway to open into a non-water tile edge.

Each canal opening into the edge of the game board counts as one point for the player, or as two points for the edge tiles marked with a blue circle.
A player's total score is the product of the scores of each of his two board sides.
For example, if 4 canals open into the upper edge and 7 into the lower edge of the board, player 1's score is 4×7=28.

The game ends when a player draws a brick that cannot be placed anywhere, or when all edge tiles are used or blocked.

## The Menu

To play a local game, both players enter their names in the text fields and click "Start!".

To host a network game, enter your name into the text field above "Host" and click "Host". The game will open and wait for another player to join.

To join a network game hosted by someone else, enter your name into the text field above "Host"
and enter the hosting computer's IP address in the text field above "Join", then click "Join".

## Saveloading

When you close a game in progress, it is saved automatically.

In the menu, click "Load Local" to load the last saved game as a local game, or "Load Multiplayer" to host a network game with the last saved game.

## Website

[Repository](https://codeberg.org/Nordfriese/Tayu)

[Bug reports go here](https://codeberg.org/Nordfriese/Tayu/issues)

## Credits

Created by [Benedikt Straub (@Nordfriese)](https://codeberg.org/Nordfriese) in his spare time. I hope you like my work.

## License

GPL3. See the file [LICENSE](https://codeberg.org/Nordfriese/Tayu/blob/master/LICENSE) for details.
