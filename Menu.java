import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class Menu extends JFrame {

	private final JTextField nameLocal1;
	private final JTextField nameLocal2;
	private final JTextField nameMP;
	private final JTextField hostname;
	private final JButton startLocal;
	private final JButton startMP;
	private final JButton join;
	private final JButton loadLocal;
	private final JButton loadMP;
	private final JButton quit;

	public void updateFields() {
		boolean canLoad = Game.kSaveFile.isFile();
		loadLocal.setEnabled(canLoad);
		loadMP.setEnabled(canLoad);
		getRootPane().setDefaultButton(canLoad ? loadLocal : startLocal);
	}

	@Override public void setVisible(boolean v) {
		super.setVisible(v);
		updateFields();
	}

	public Menu() {
		super("Tayü");

		GridLayout layout = new GridLayout(0, 1);
		layout.setHgap(4);
		Border border = new EtchedBorder(EtchedBorder.RAISED);
		JPanel mainbox = new JPanel(layout);

		JLabel l = new JLabel("Main Menu", JLabel.CENTER);
		l.setFont(new Font(Font.SERIF, Font.BOLD, 20));
		l.setBorder(border);
		mainbox.add(l);

		nameLocal1 = new JTextField("Player 1");
		nameLocal2 = new JTextField("Player 2");
		nameMP = new JTextField("Player");
		hostname = new JTextField("127.0.0.1");

		for (JTextField txt : new JTextField[] { nameLocal1, nameLocal2, nameMP, hostname }) {
			txt.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));
			txt.setColumns(30);
		}

		startLocal = new JButton("Start!");
		startMP = new JButton("Host");
		join = new JButton("Join");
		loadLocal = new JButton("Load Local");
		loadMP = new JButton("Load Multiplayer");
		quit = new JButton("Quit!");

		startLocal.setMnemonic(KeyEvent.VK_S);
		startMP.setMnemonic(KeyEvent.VK_H);
		join.setMnemonic(KeyEvent.VK_J);
		loadLocal.setMnemonic(KeyEvent.VK_L);
		loadMP.setMnemonic(KeyEvent.VK_M);
		quit.setMnemonic(KeyEvent.VK_Q);

		quit.addActionListener((e) -> {
			System.exit(0);
		});
		startLocal.addActionListener((e) -> {
			setVisible(false);
			new Game(this, nameLocal1.getText(), nameLocal2.getText(), false, Game.GameType.kLocal);
		});
		startMP.addActionListener((e) -> {
			setVisible(false);
			new Game(this, nameMP.getText(), null, false, Game.GameType.kHost);
		});
		join.addActionListener((e) -> {
			setVisible(false);
			new Game(this, hostname.getText(), nameMP.getText(), false, Game.GameType.kClient);
		});
		loadLocal.addActionListener((e) -> {
			setVisible(false);
			new Game(this, nameLocal1.getText(), nameLocal2.getText(), true, Game.GameType.kLocal);
		});
		loadMP.addActionListener((e) -> {
			setVisible(false);
			new Game(this, nameMP.getText(), null, true, Game.GameType.kHost);
		});

		mainbox.add(nameLocal1);
		mainbox.add(nameLocal2);
		mainbox.add(startLocal);
		mainbox.add(new JSeparator());

		JPanel p = new JPanel(new GridLayout(1, 0));
		p.add(loadLocal);
		p.add(loadMP);
		mainbox.add(p);
		mainbox.add(new JSeparator());

		mainbox.add(nameMP);
		mainbox.add(startMP);
		mainbox.add(hostname);
		mainbox.add(join);
		mainbox.add(new JSeparator());

		mainbox.add(quit);
		add(mainbox, BorderLayout.CENTER);

		updateFields();
		setResizable(false);
		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}

}
